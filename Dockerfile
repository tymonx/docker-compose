# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ARG DOCKER_VERSION=stable
FROM docker:${DOCKER_VERSION} as docker

FROM docker as builder

# Install needed packages for building docker-compose
# Update package installer for Python (pip) and wheel
# Build the docker-compose
RUN apk add --no-cache \
        python-dev \
        libffi-dev \
        openssl-dev \
        libc-dev \
        py-pip \
        make \
        gcc && \
    pip install --upgrade pip wheel && \
    pip wheel --wheel-dir=/wheel docker-compose

FROM docker

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF

# Build-time metadata as defined at http://label-schema.org
LABEL \
    maintainer="tymoteusz.blazejczyk.pl@gmail.com" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.build-date=$BUILD_DATE \
    org.label-schema.name="tymonx/docker-compose" \
    org.label-schema.description="A Docker image with the docker-compose tool" \
    org.label-schema.usage="https://gitlab.com/tymonx/docker-compose/README.md" \
    org.label-schema.url="https://gitlab.com/tymonx/docker-compose" \
    org.label-schema.vcs-url="https://gitlab.com/tymonx/docker-compose" \
    org.label-schema.vcs-ref=$VCS_REF \
    org.label-schema.vendor="tymonx" \
    org.label-schema.version=$VERSION \
    org.label-schema.docker.cmd=\
"docker run --rm --user $(id -u):$(id -g) --volume $(pwd):$(pwd) --workdir $(pwd) --entrypoint /bin/sh tymonx/docker-compose"

COPY --from=builder /wheel /wheel

# Install the docker-compose
RUN apk add --no-cache \
        python \
        py-pip \
        bash && \
    pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir --no-index --find-links=/wheel \
        docker-compose && \
    apk del --purge py-pip && \
    rm -rf /wheel && \
    rm -rf /root/.cache/pip/*
